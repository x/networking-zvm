..
      Copyright 2019 IBM
      All Rights Reserved.

      Licensed under the Apache License, Version 2.0 (the "License"); you may
      not use this file except in compliance with the License. You may obtain
      a copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
      WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
      License for the specific language governing permissions and limitations
      under the License.

Welcome to networking-zvm's documentation!
==========================================

This project implements Neutron ML2 zvm mechnism driver and zvm network agent.

Overview
--------

.. toctree::
   :maxdepth: 1
   
   overview

Installation
------------

.. toctree::
   :maxdepth: 1
   
   installation
   
Usage
-----

.. toctree::
   :maxdepth: 3
   
   usage

Sample cases
------------

.. toctree::
   :maxdepth: 2

   single_flat
   single_vlan
   mixed
   multiple

Links
-----

* Documentation: `<https://networking-zvm.readthedocs.io/en/latest/>`_
* Source: `<http://git.openstack.org/cgit/openstack/networking-zvm>`_
* Github shadow: `<https://github.com/openstack/networking-zvm>`_
* Bugs: `<http://bugs.launchpad.net/networking-zvm>`_
* Gerrit: `<https://review.openstack.org/#/q/project:openstack/networking-zvm>`_
