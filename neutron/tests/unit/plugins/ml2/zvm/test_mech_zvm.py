# =================================================================
# Licensed Materials - Property of IBM
#
# (c) Copyright IBM Corp. 2014, 2015 All Rights Reserved
#
# US Government Users Restricted Rights - Use, duplication or
# disclosure restricted by GSA ADP Schedule Contract with IBM Corp.
# =================================================================
# All Rights Reserved.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

import mock

from neutron.plugins.ml2.drivers.zvm import mech_zvm
from neutron.tests.unit import fake_resources as fakes
from neutron.tests.unit.plugins.ml2 import _test_mech_agent as base
from neutron.tests.unit.plugins.ml2 import test_plugin

from neutron_lib.api.definitions import portbindings
from neutron_lib import constants as const


class ZvmMechanismBaseTestCase(base.AgentMechanismBaseTestCase,
                               test_plugin.Ml2PluginV2TestCase):
    VIF_TYPE = mech_zvm.VIF_TYPE_ZVM
    CAP_PORT_FILTER = False
    AGENT_TYPE = mech_zvm.AGENT_TYPE_ZVM

    GOOD_MAPPINGS = {'fake_physical_network': 'fake_vswitch'}
    GOOD_CONFIGS = {'vswitch_mappings': GOOD_MAPPINGS}

    BAD_MAPPINGS = {'wrong_physical_network': 'wrong_vswitch'}
    BAD_CONFIGS = {'vswitch_mappings': BAD_MAPPINGS}

    AGENTS = [{'alive': True,
               'configurations': GOOD_CONFIGS,
               'host': 'host',
               'agent_type': AGENT_TYPE}]
    AGENTS_DEAD = [{'alive': False,
                    'configurations': GOOD_CONFIGS,
                    'host': 'dead_host',
                    'agent_type': AGENT_TYPE}]
    AGENTS_BAD = [{'alive': False,
                   'configurations': GOOD_CONFIGS,
                   'host': 'bad_host_1',
                   'agent_type': AGENT_TYPE},
                  {'alive': True,
                   'configurations': BAD_CONFIGS,
                   'host': 'bad_host_2',
                   'agent_type': AGENT_TYPE}]

    def setUp(self):
        super(ZvmMechanismBaseTestCase, self).setUp()
        self.driver = mech_zvm.ZvmMechanismDriver()
        self.driver.initialize()


class ZvmMechanismGenericTestCase(ZvmMechanismBaseTestCase,
                                  base.AgentMechanismGenericTestCase):
    pass


class ZvmMechanismLocalTestCase(ZvmMechanismBaseTestCase,
                                base.AgentMechanismLocalTestCase):
    pass


class ZvmvMechanismFlatTestCase(ZvmMechanismBaseTestCase,
                                base.AgentMechanismFlatTestCase):
    pass


class ZvmvMechanismVlanTestCase(ZvmMechanismBaseTestCase,
                                base.AgentMechanismVlanTestCase):
    pass


class TestZvmMechanismDriver(ZvmMechanismBaseTestCase):

    def test_update_port_postcommit(self):
        fake_port = fakes.FakePort.create_one_port(
            attrs={'status': const.PORT_STATUS_ACTIVE}).info()
        fake_ctx = mock.Mock(current=fake_port, original=fake_port)
        self.driver._plugin.update_port_status = mock.Mock()

        self.driver.update_port_postcommit(fake_ctx)
        self.driver._plugin.update_port_status.assert_not_called()

    def test_update_port_postcommit_live_migration(self):
        fake_context = 'fake_context'
        fake_port = fakes.FakePort.create_one_port(
            attrs={
                'status': const.PORT_STATUS_DOWN,
                portbindings.PROFILE: {mech_zvm.MIGRATING_ATTR: 'foo'},
                portbindings.VIF_TYPE: mech_zvm.VIF_TYPE_ZVM}).info()

        fake_ctx = mock.Mock(current=fake_port, original=fake_port,
                             _plugin_context=fake_context)
        self.driver._plugin.update_port_status = mock.Mock()

        self.driver.update_port_postcommit(fake_ctx)
        self.driver._plugin.update_port_status.assert_called_once_with(
                fake_context, fake_port['id'], const.PORT_STATUS_ACTIVE)
