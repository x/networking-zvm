..
      Copyright 2019 IBM
      All Rights Reserved.

      Licensed under the Apache License, Version 2.0 (the "License"); you may
      not use this file except in compliance with the License. You may obtain
      a copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
      WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
      License for the specific language governing permissions and limitations
      under the License.

Installation
************

At the command line::

    pip install neutron-zvm-plugin

After this command, there is a binary available: "/usr/bin/neutron-zvm-agent".
Then create a file named "neutron-zvm-agent.service" under "/usr/lib/systemd/
system/" with following content, and use "systemctl enable neutron-zvm-agent",
 "systemctl start neutron-zvm-agent" to enable and start the service.

neutron-zvm-agent.service
^^^^^^^^^^^^^^^^^^^^^^^^^

[Unit]
Description=OpenStack Neutron zVM Plugin
After=syslog.target network.target

[Service]
Type=simple
User=neutron
ExecStart=/usr/bin/neutron-zvm-agent --config-file /etc/neutron/neutron.conf --config-file /etc/neutron/plugins/ml2/ml2_conf.ini --config-file /etc/neutron/plugins/zvm/neutron_zvm_plugin.ini --log-file /var/log/neutron/zvm-agent.log

[Install]
WantedBy=multi-user.target
