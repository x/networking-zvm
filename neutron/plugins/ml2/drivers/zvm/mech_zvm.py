# Copyright 2014 IBM Corp.
#
# All Rights Reserved.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

import copy

from neutron_lib.api.definitions import portbindings
from neutron_lib import constants
from neutron_lib.plugins import directory
from oslo_log import log as logging

from neutron.plugins.ml2.drivers import mech_agent


LOG = logging.getLogger(__name__)
AGENT_TYPE_ZVM = 'z/VM agent'
VIF_TYPE_ZVM = 'zvm'
MIGRATING_ATTR = 'migrating_to'


class ZvmMechanismDriver(mech_agent.SimpleAgentMechanismDriverBase):
    """Attach to networks using zvm vswitch agent.

    The ZvmMechanismDriver integrates the ml2 plugin with the
    z/VM L2 agent. Port binding with this driver requires the
    z/VM vswitch agent to be running on the port's host, and that agent
    to have connectivity to at least one segment of the port's network.
    """

    def __init__(self):
        super(ZvmMechanismDriver, self).__init__(
            AGENT_TYPE_ZVM,
            VIF_TYPE_ZVM,
            {portbindings.CAP_PORT_FILTER: False})
        self._plugin_property = None

    @property
    def _plugin(self):
        if self._plugin_property is None:
            self._plugin_property = directory.get_plugin()
        return self._plugin_property

    def get_allowed_network_types(self, agent=None):
        return [constants.TYPE_LOCAL, constants.TYPE_FLAT,
                constants.TYPE_VLAN]

    def get_mappings(self, agent):
        return agent['configurations'].get('vswitch_mappings', {})

    def update_port_postcommit(self, context):
        """Update a port.

        :param context: PortContext instance describing the new
        state of the port, as well as the original state prior
        to the update_port call.

        Called after the transaction completes. Call can block, though
        will block the entire process so care should be taken to not
        drastically affect performance.  Raising an exception will
        result in the deletion of the resource.

        update_port_postcommit is called for all changes to the port
        state. It is up to the mechanism driver to ignore state or
        state changes that it does not know or care about.

        For zvm mechnism driver, this action is taken to update
        a port to active after live migration happens.
        """
        port = copy.deepcopy(context.current)
        original_port = copy.deepcopy(context.original)

        if (port['status'] == constants.PORT_STATUS_DOWN and
            port[portbindings.VIF_TYPE] == VIF_TYPE_ZVM and
            (MIGRATING_ATTR in port[portbindings.PROFILE].keys() or
             MIGRATING_ATTR in original_port[portbindings.PROFILE].keys())):
            LOG.info("Setting port %s status from DOWN to UP after "
                     "migration completes.",
                     port['id'])
            self._plugin.update_port_status(context._plugin_context,
                                            port['id'],
                                            constants.PORT_STATUS_ACTIVE)
        return
